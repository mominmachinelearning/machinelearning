clear all;
clc;
x = importdata('q1x.dat');
y = importdata('q1y.dat');

mean = sum(x)/length(x);
sd = sqrt(((x-mean)' * (x-mean))/length(x));

%mean = 8.1598;
%sd = 3.8499;

for i = 1:length(x)
    x(i) = ((x(i) - mean) / sd);
end

plot(x,y,'o');
ylabel('price');
xlabel('Area');

optimumtheta = gradientdescent(x,y,0.0000000001)

hold on;
plot(x(:,1),optimumtheta(1,1)+optimumtheta(2,1)*x);

function theta = gradientdescent(x,y,epsilon)
%batch gradient descent algorithm is used for linear regression
%x(mx1) y(mx1) theta(2x1) X(mx2)
    theta = [0;0];
    thetaprev = theta;
    m = length(y);
    X = [ones(m,1),x];

    %{
    theta0vals = linspace(0, 10,100);
    theta1vals = linspace(0, 10,100);
    jtheta = zeros(length(theta0vals), length(theta1vals));  
    for i = 1:length(theta0vals)
          for j = 1:length(theta1vals)
            theta_val_vec = [theta0vals(i);theta1vals(j)];
            htheta = (X*theta_val_vec);
            jtheta(i,j) = (sum((htheta - y).^2))/(2*m);
          end
    end
    figure();
    %mesh(theta0vals,theta1vals,jtheta);
    contour(theta0vals,theta1vals,jtheta);
    xlabel('theta0'); ylabel('theta1');zlabel('Jtheta');
    title('J(theta)');
    %}
    
    notconverged = true;
    cost = 0;
    while notconverged
        cost = cost + 1;
        thetaprev = theta;
        h = X * thetaprev;
        err = h - y;
        gradient = (1 / (m*cost)) * (X' * err); %eta = 1/cost , figures are using eta=0.01
        theta = thetaprev - gradient;
        notconverged = abs(sum((y - (X * theta)).^2) - sum((y - (X * thetaprev)).^2))/(2*m) > epsilon;
       
        
        %hold on;
        %plot3(theta(2),theta(1),(sum((y - (X * theta)).^2))/(2*m),'ro');
        %pause(0.2);
    end
    cost
    
end
