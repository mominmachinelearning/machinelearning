clear all;
clc;
x = importdata('q4x.dat');
y = importdata('q4y.dat');

mean = mean(x);
sd = sqrt((sum((x-mean).^2))/length(y));

x(:,1) = ((x(:,1) - mean(:,1)) / sd(:,1));
x(:,2) = ((x(:,2) - mean(:,2)) / sd(:,2));

for i=1:length(y)
    if strcmp(y(i),'Alaska') == 1
        hold on;
        plot(x(i,1),x(i,2),'rx');
    else
        hold on;
        plot(x(i,1),x(i,2),'bo');
    end
end
    
[phi,mean1,mean0,covariance] = gdafun(x,y)


function [phi,mean1,mean0,covariance] = gdafun(x,y)
%gaussian discriminant analysis algorithm is used for classification
    phi = 0;
    mean1=[0 0];
    mean0=[0 0];
    for i=1:length(y)
        if strcmp(y(i),'Alaska')==1
            phi = phi + 1;
            mean1 = mean1 + x(i,:);
        else
            mean0 = mean0 + x(i,:);
        end
    
    end
    mean1 = mean1 / (phi);
    mean0 = mean0 / (length(y)-phi);
    phi = (phi)/(length(y));
    
    covariance = [0 0;0 0];
    
    for i=1:length(y)
        if strcmp(y(i),'Alaska')==1
            covariance = covariance + (x(i,:)-mean1)'*(x(i,:)-mean1);
        else
            covariance = covariance + (x(i,:)-mean0)'*(x(i,:)-mean0);
        end
    end
    covariance = covariance / length(y)
    
    invcov = inv(covariance);
    
    theta0 = 0.5*((mean0 * invcov * mean0') - (mean1 * invcov * mean1')) - log(phi/(1-phi));
    theta1 = (mean1-mean0) * invcov;
    
    db = @(x,y) (theta1(1,1)*x + theta1(1,2)*y) + theta0;
    hold on;
    ezplot(db);
end
