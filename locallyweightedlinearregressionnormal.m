clear all;
clc;
x = importdata('q3x.dat');
y = importdata('q3y.dat');

mean = sum(x)/length(x);
sd = sqrt(((x-mean)' * (x-mean))/length(x));

for i = 1:length(x)
    x(i) = ((x(i) - mean) / sd);
end

plot(x,y,'o');
ylabel('price');
xlabel('Area');

for i=1:length(x)
    optimumtheta = normalequation(x,y,10,x(i))
    hold on;
    plot(x(i),optimumtheta(1,1)+optimumtheta(2,1)*x(i),'rx-');
end 

function theta = normalequation(x,y,tau,querypoint)
%locally weighted normal equation algorithm is used for linear regression
    theta = [0;0];
    m = length(x);
    X = [ones(m,1),x];
    W = zeros(m,m);
    
    for i = 1:m
        W(i,i) = exp(((querypoint-x(i))^2)/(-2*((tau)^2)));
    end
    
    theta = (X' * W * X)\(X' * W * y);
    
end