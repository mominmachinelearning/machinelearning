clear all;
clc;
x = importdata('q4x.dat');
y = importdata('q4y.dat');

mean = mean(x);
sd = sqrt((sum((x-mean).^2))/length(y));

x(:,1) = ((x(:,1) - mean(:,1)) / sd(:,1));
x(:,2) = ((x(:,2) - mean(:,2)) / sd(:,2));

for i=1:length(y)
    if strcmp(y(i),'Alaska') == 1
        hold on;
        plot(x(i,1),x(i,2),'rx');
    else
        hold on;
        plot(x(i,1),x(i,2),'bo');
    end
end
    
[phi,mean1,mean0,covariance0,covariance1] = gdafun(x,y)


function [phi,mean1,mean0,covariance0,covariance1] = gdafun(x,y)
%gaussian discriminant analysis algorithm is used for classification
    phi = 0;
    mean1=[0 0];
    mean0=[0 0];

    for i=1:length(y)
        if strcmp(y(i),'Alaska')==1
            phi = phi + 1;
            mean1 = mean1 + x(i,:);
        else
            mean0 = mean0 + x(i,:);
        end
    
    end
    mean1 = mean1 / (phi);
    mean0 = mean0 / (length(y)-phi);
    phi = (phi)/(length(y));
    
    covariance0 = [0 0;0 0];
    covariance1 = [0 0;0 0];
    
    c1 = 0;
    c0 = 0;
    
    for i=1:length(y)
        if strcmp(y(i),'Alaska')==1
            covariance1 = covariance1 + (x(i,:)-mean1)'*(x(i,:)-mean1);
            c1 = c1 + 1;
        else
            covariance0 = covariance0 + (x(i,:)-mean0)'*(x(i,:)-mean0);
            c0 = c0 + 1;
        end
    end
    covariance0 = covariance0 / c0
    covariance1 = covariance1 / c1
    
    invcov0 = inv(covariance0);
    invcov1 = inv(covariance1);
    
    theta0 = ((mean0 * invcov0 * mean0') - (mean1 * invcov1 * mean1')) + (2*log(phi/(1-phi))) + log(det(covariance0)/det(covariance1));
    theta1 = (invcov1*mean1' - invcov0*mean0')';
    theta2 = invcov0 - invcov1;
    
    db = @(x,y) (x*x*theta2(1,1)) + (x*y*(theta2(2,1) + theta2(1,2))) + (y*y*theta2(2,2)) + (theta1(1,1)*x + theta1(1,2)*y) + theta0;
    hold on;
    ezplot(db);
end
