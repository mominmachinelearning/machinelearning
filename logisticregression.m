clear all;
clc;
x = importdata('q2x.dat');
y = importdata('q2y.dat');

mean = mean(x);
sd = sqrt(sum((x-mean).^2))/length(y);

x(:,1) = ((x(:,1) - mean(:,1)) / sd(:,1));
x(:,2) = ((x(:,2) - mean(:,2)) / sd(:,2));

for i=1:length(y)
    if y(i) == 1
        hold on;
        plot(x(i,1),x(i,2),'rx');
    else
        hold on;
        plot(x(i,1),x(i,2),'bo');
    end
end
    
optimumtheta = newtonsmethod(x,y,0.0000000001)


function theta = newtonsmethod(x,y,epsilon)
%newtons method algorithm is used for logistic regression
%x(mx2) y(mx1) theta(3x1) X(mx3)
    theta = [0;0;0];
    thetaprev = theta;
    m = length(y);
    X = [ones(m,1),x];
    
    notconverged = true;
    cost = 0;
    while notconverged
        cost = cost + 1;
        thetaprev = theta;
        h = sigmf(X * thetaprev, [1 0]);
        err = h - y;
        hessianinv = inv(X' * (diag(h.*(1-h))) * X);
        gradient = (hessianinv) * (X' * err); %eta = 1/cost , figures are using eta=0.01
        theta = thetaprev - gradient;
        htheta = sigmf(X * theta, [1 0]);
        notconverged = abs(sum((y - htheta).^2) - sum((y - h).^2))/(2*m) > epsilon;
        
    end
    cost
    hold on;
    plot(X(:,2), -(theta(1) + (theta(2)*X(:,2)))/theta(3));
end
