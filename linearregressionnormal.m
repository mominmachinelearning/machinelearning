clear all;
clc;
x = importdata('q3x.dat');
y = importdata('q3y.dat');

mean = sum(x)/length(x);
sd = sqrt(((x-mean)' * (x-mean))/length(x));

%mean = 8.1598;
%sd = 3.8499;

for i = 1:length(x)
    x(i) = ((x(i) - mean) / sd);
end

plot(x,y,'o');
ylabel('price');
xlabel('Area');

optimumtheta = normalequation(x,y)

hold on;
plot(x(:,1),optimumtheta(1,1)+optimumtheta(2,1)*x);

function theta = normalequation(x,y)
%normal equation algorithm is used for linear regression
%x(mx1) y(mx1) theta(2x1) X(mx2)
    theta = [0;0];
    m = length(x);
    X = [ones(m,1),x];
    theta = (X' * X)\(X' * y);
    
end
